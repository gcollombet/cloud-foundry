{
  "name": "Cloud Foundry",
  "description": "Deploy your application to a [Cloud Foundry](https://www.cloudfoundry.org/) platform",
  "template_path": "templates/gitlab-ci-cf.yml",
  "kind": "hosting",
  "variables": [
    {
      "name": "CF_CLI_IMAGE",
      "description": "The Docker image used to run CF CLI commands - **set the version required by your Cloud Foundry server**",
      "default": "registry.hub.docker.com/governmentpaas/cf-cli"
    },
    {
      "name": "CF_MANIFEST_BASENAME",
      "description": "CF manifest file basename (without extension nor env suffix)",
      "default": "manifest",
      "advanced": true
    },
    {
      "name": "CF_URL",
      "type": "url",
      "description": "Global Cloud Foundry API url",
      "mandatory": true
    },
    {
      "name": "CF_ORG",
      "description": "Global Cloud Foundry organization for project",
      "mandatory": true
    },
    {
      "name": "CF_DEFAULT_DOMAIN",
      "description": "Global Cloud Foundry default CF domain _(only define if you want to use a different domain from CF default)_",
      "advanced": true
    },
    {
      "name": "CF_USER",
      "description": "Global Cloud Foundry username",
      "secret": true,
      "mandatory": true
    },
    {
      "name": "CF_PASSWORD",
      "description": "Global Cloud Foundry password",
      "secret": true,
      "mandatory": true
    },
    {
      "name": "CF_BASE_APP_NAME",
      "description": "Base application name",
      "default": "$CI_PROJECT_NAME",
      "advanced": true
    },
    {
      "name": "CF_SCRIPTS_DIR",
      "description": "directory where Cloud Foundry scripts (manifest, hook scripts) are located",
      "default": ".",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "review",
      "name": "Review",
      "description": "Dynamic review environments for your topic branches (see GitLab [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/))",
      "variables": [
        {
          "name": "CF_REVIEW_SPACE",
          "description": "Cloud Foundry space for review env",
          "mandatory": true
        },
        {
          "name": "CF_REVIEW_APP_NAME",
          "description": "The application name for review env (only define if different from global)",
          "advanced": true
        },
        {
          "name": "CF_REVIEW_ENVIRONMENT_SCHEME",
          "description": "The review environment protocol scheme",
          "default": "https",
          "mandatory": true
        },
        {
          "name": "CF_REVIEW_ENVIRONMENT_DOMAIN",
          "description": "The review environment domain (ex: `noprod-cloudfoundry.domain.com`).\n\nBy default review `environment.url` will be built as `${CF_REVIEW_ENVIRONMENT_SCHEME}://${$CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${CF_REVIEW_ENVIRONMENT_DOMAIN}`",
          "mandatory": true
        },
        {
          "name": "CF_REVIEW_ZERODOWNTIME",
          "type": "boolean",
          "description": "Enables zero-downtime deployment on review env",
          "advanced": true
        },
        {
          "name": "CF_REVIEW_URL",
          "type": "url",
          "description": "Cloud Foundry API url for review env (only define if different from global)",
          "advanced": true
        },
        {
          "name": "CF_REVIEW_USER",
          "description": "Cloud Foundry API username for review env (only define if different from global)",
          "secret": true
        },
        {
          "name": "CF_REVIEW_PASSWORD",
          "description": "Cloud Foundry API password for review env (only define if different from global)",
          "secret": true
        },
        {
          "name": "CLEANUP_ALL_REVIEW",
          "description": "Enables a **manual** job to cleanup all review envs at once.\n\nYou may also use it to [schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) cloud resources cleanup. See documentation.",
          "type": "boolean"
        },
        {
          "name": "CF_REVIEW_RETIRED_APP_SUFFIX",
          "description": "If set, the app old version is not deleted/overriden but renamed with this suffix",
          "advanced": true
        }
      ]
    },
    {
      "id": "integration",
      "name": "Integration",
      "description": "A continuous-integration environment associated to your integration branch (`develop` by default)",
      "variables": [
        {
          "name": "CF_INTEG_SPACE",
          "description": "Cloud Foundry space for integration env",
          "mandatory": true
        },
        {
          "name": "CF_INTEG_APP_NAME",
          "description": "The application name for integration env (only define if different from global)",
          "advanced": true
        },
        {
          "name": "CF_INTEG_ENVIRONMENT_URL",
          "type": "url",
          "description": "The integration environment url including scheme (ex: `https://my-application-integration.noprod-cloudfoundry.domain.com`).\n\nDo not use variable inside variable definition as it will result in a two level cascade variable and gitlab does not allow that.",
          "mandatory": true
        },
        {
          "name": "CF_INTEG_ZERODOWNTIME",
          "type": "boolean",
          "description": "Enables zero-downtime deployment on integration env",
          "advanced": true
        },
        {
          "name": "CF_INTEG_URL",
          "type": "url",
          "description": "Cloud Foundry API url for integration env (only define if different from global)",
          "advanced": true
        },
        {
          "name": "CF_INTEG_USER",
          "description": "Cloud Foundry API username for integration env (only define if different from global)",
          "secret": true
        },
        {
          "name": "CF_INTEG_PASSWORD",
          "description": "Cloud Foundry API password for integration env (only define if different from global)",
          "secret": true
        },
        {
          "name": "CF_INTEG_RETIRED_APP_SUFFIX",
          "description": "If set, the app old version is not deleted/overriden but renamed with this suffix",
          "advanced": true
        }
      ]
    },
    {
      "id": "staging",
      "name": "Staging",
      "description": "An iso-prod environment meant for testing and validation purpose on your production branch (`master` by default)",
      "variables": [
        {
          "name": "CF_STAGING_SPACE",
          "description": "Cloud Foundry space for staging env",
          "mandatory": true
        },
        {
          "name": "CF_STAGING_APP_NAME",
          "description": "The application name for staging env (only define if different from global)",
          "advanced": true
        },
        {
          "name": "CF_STAGING_ENVIRONMENT_URL",
          "type": "url",
          "description": "The staging environment url including scheme (ex: `https://my-application-staging.noprod-cloudfoundry.domain.com`).\n\nDo not use variable inside variable definition as it will result in a two level cascade variable and gitlab does not allow that.",
          "mandatory": true
        },
        {
          "name": "CF_STAGING_ZERODOWNTIME",
          "type": "boolean",
          "description": "Enables zero-downtime deployment on staging env",
          "advanced": true
        },
        {
          "name": "CF_STAGING_URL",
          "type": "url",
          "description": "Cloud Foundry API url for staging env (only define if different from global)",
          "advanced": true
        },
        {
          "name": "CF_STAGING_USER",
          "description": "Cloud Foundry API username for staging env (only define if different from global)",
          "secret": true
        },
        {
          "name": "CF_STAGING_PASSWORD",
          "description": "Cloud Foundry API password for staging env (only define if different from global)",
          "secret": true
        },
        {
          "name": "CF_STAGING_RETIRED_APP_SUFFIX",
          "description": "If set, the app old version is not deleted/overriden but renamed with this suffix",
          "advanced": true
        }
      ]
    },
    {
      "id": "prod",
      "name": "Production",
      "description": "The production environment",
      "variables": [
        {
          "name": "CF_PROD_SPACE",
          "description": "Cloud Foundry space for production env",
          "mandatory": true
        },
        {
          "name": "CF_PROD_APP_NAME",
          "description": "The application name for production env (only define if different from global)",
          "advanced": true
        },
        {
          "name": "CF_PROD_ENVIRONMENT_URL",
          "type": "url",
          "description": "The production environment url including scheme (ex: `https://my-application.cloudfoundry.domain.com`).\n\nDo not use variable inside variable definition as it will result in a two level cascade variable and gitlab does not allow that.",
          "mandatory": true
        },
        {
          "name": "CF_PROD_DEPLOY_STRATEGY",
          "description": "Defines the deployment to production strategy.",
          "type": "enum",
          "values": ["manual", "auto"],
          "default": "manual"
        },
        {
          "name": "CF_PROD_ZERODOWNTIME",
          "type": "boolean",
          "description": "Enables zero-downtime deployment on production env",
          "advanced": true
        },
        {
          "name": "CF_PROD_URL",
          "type": "url",
          "description": "Cloud Foundry API url for production env (only define if different from global)",
          "advanced": true
        },
        {
          "name": "CF_PROD_USER",
          "description": "Cloud Foundry API username for production env (only define if different from global)",
          "secret": true
        },
        {
          "name": "CF_PROD_PASSWORD",
          "description": "Cloud Foundry API password for production env (only define if different from global)",
          "secret": true
        },
        {
          "name": "CF_PROD_RETIRED_APP_SUFFIX",
          "description": "If set, the app old version is not deleted/overriden but renamed with this suffix",
          "advanced": true
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-cf-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "$CI_REGISTRY/to-be-continuous/tools/vault-secrets-provider:master",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
