#!/usr/bin/env bash
if [[ -z "$appname" ]]
then
  echo "[ERROR] appname env not passed"
  exit 1
fi

if [[ -z "$tmpappname" ]]
then
  echo "[ERROR] tmpappname env not passed"
  exit 1
fi

echo "pre-push hook called for $appname/$tmpappname"
